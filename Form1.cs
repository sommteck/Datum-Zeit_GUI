using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Datm_Zeit_GUI
{
    public partial class day_time : Form
    {
        DateTime dt = new DateTime();

        public day_time()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_today_Click(object sender, EventArgs e)
        {
            // Systemdatum ermitteln
            dt = DateTime.Today;
            lbl_day.Text = "Kurze Datumsausgabe: " + dt.ToShortDateString() +
                "\nLange Datumsausgabe: " + dt.ToLongDateString();
        }

        private void cmd_time_Click(object sender, EventArgs e)
        {
            dt = DateTime.Now;
            lbl_time.Text = "Kurze Zeitangabe: " + dt.ToShortTimeString() +
                "\nLange Zeitangabe: " + dt.ToLongTimeString() +
                "\nOrtszeit: " + dt.ToLocalTime();
        }

        private void cmd_leap_year_Click(object sender, EventArgs e)
        {
            dt = DateTime.Now;
            if (DateTime.IsLeapYear(dt.Year))
                lbl_leap_year.Text = "Übergebenes Jahr ist ein Schaltjahr";
            else
                lbl_leap_year.Text = "Übergebenes Jahr ist kein Schaltjahr";
        }

        private void cmd_daysinmounth_Click(object sender, EventArgs e)
        {
            try
            {
                lbl_days_of_mounth.Text = "Tage im Monat: " + DateTime.DaysInMonth
                    (Convert.ToInt32(txt_year.Text), Convert.ToInt32(txt_mounth.Text));
            }
            catch
            {
                MessageBox.Show("Zuerst Jahr und Monat eingeben!");
                txt_mounth.Text = txt_year.Text = null;
                txt_mounth.Focus();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lbl_dtp.Text = "Datum: " + Convert.ToString(dateTimePicker1.Value.ToShortDateString()) +
                "\nZeit: " + Convert.ToString(dateTimePicker1.Value.ToShortTimeString());

            txt_mounth.Text = (dateTimePicker1.Value.ToShortDateString()).Substring(3, 2);
            txt_year.Text = (dateTimePicker1.Value.ToShortDateString()).Substring(6, 4);
        }
    }
}