namespace Datm_Zeit_GUI
{
    partial class day_time
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_today = new System.Windows.Forms.Button();
            this.cmd_time = new System.Windows.Forms.Button();
            this.cmd_leap_year = new System.Windows.Forms.Button();
            this.cmd_daysinmounth = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.lbl_day = new System.Windows.Forms.Label();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_leap_year = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_days_of_mounth = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_year = new System.Windows.Forms.TextBox();
            this.txt_mounth = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lbl_dtp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmd_today
            // 
            this.cmd_today.Location = new System.Drawing.Point(31, 42);
            this.cmd_today.Name = "cmd_today";
            this.cmd_today.Size = new System.Drawing.Size(75, 23);
            this.cmd_today.TabIndex = 0;
            this.cmd_today.Text = "To&Day";
            this.cmd_today.UseVisualStyleBackColor = true;
            this.cmd_today.Click += new System.EventHandler(this.cmd_today_Click);
            // 
            // cmd_time
            // 
            this.cmd_time.Location = new System.Drawing.Point(31, 104);
            this.cmd_time.Name = "cmd_time";
            this.cmd_time.Size = new System.Drawing.Size(75, 23);
            this.cmd_time.TabIndex = 1;
            this.cmd_time.Text = "&Time";
            this.cmd_time.UseVisualStyleBackColor = true;
            this.cmd_time.Click += new System.EventHandler(this.cmd_time_Click);
            // 
            // cmd_leap_year
            // 
            this.cmd_leap_year.Location = new System.Drawing.Point(31, 172);
            this.cmd_leap_year.Name = "cmd_leap_year";
            this.cmd_leap_year.Size = new System.Drawing.Size(75, 23);
            this.cmd_leap_year.TabIndex = 2;
            this.cmd_leap_year.Text = "Schaltjahr";
            this.cmd_leap_year.UseVisualStyleBackColor = true;
            this.cmd_leap_year.Click += new System.EventHandler(this.cmd_leap_year_Click);
            // 
            // cmd_daysinmounth
            // 
            this.cmd_daysinmounth.Location = new System.Drawing.Point(245, 232);
            this.cmd_daysinmounth.Name = "cmd_daysinmounth";
            this.cmd_daysinmounth.Size = new System.Drawing.Size(137, 27);
            this.cmd_daysinmounth.TabIndex = 3;
            this.cmd_daysinmounth.Text = "Tage des &Monats";
            this.cmd_daysinmounth.UseVisualStyleBackColor = true;
            this.cmd_daysinmounth.Click += new System.EventHandler(this.cmd_daysinmounth_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(515, 42);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 4;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // lbl_day
            // 
            this.lbl_day.AutoSize = true;
            this.lbl_day.Location = new System.Drawing.Point(164, 42);
            this.lbl_day.Name = "lbl_day";
            this.lbl_day.Size = new System.Drawing.Size(0, 16);
            this.lbl_day.TabIndex = 5;
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Location = new System.Drawing.Point(167, 104);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(0, 16);
            this.lbl_time.TabIndex = 6;
            // 
            // lbl_leap_year
            // 
            this.lbl_leap_year.AutoSize = true;
            this.lbl_leap_year.Location = new System.Drawing.Point(167, 172);
            this.lbl_leap_year.Name = "lbl_leap_year";
            this.lbl_leap_year.Size = new System.Drawing.Size(0, 16);
            this.lbl_leap_year.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Jahr";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Monat";
            // 
            // lbl_days_of_mounth
            // 
            this.lbl_days_of_mounth.AutoSize = true;
            this.lbl_days_of_mounth.Location = new System.Drawing.Point(242, 283);
            this.lbl_days_of_mounth.Name = "lbl_days_of_mounth";
            this.lbl_days_of_mounth.Size = new System.Drawing.Size(0, 16);
            this.lbl_days_of_mounth.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 338);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "DateTimePicker";
            // 
            // txt_year
            // 
            this.txt_year.Location = new System.Drawing.Point(105, 232);
            this.txt_year.Name = "txt_year";
            this.txt_year.Size = new System.Drawing.Size(100, 22);
            this.txt_year.TabIndex = 12;
            // 
            // txt_mounth
            // 
            this.txt_mounth.Location = new System.Drawing.Point(105, 283);
            this.txt_mounth.Name = "txt_mounth";
            this.txt_mounth.Size = new System.Drawing.Size(100, 22);
            this.txt_mounth.TabIndex = 13;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(196, 338);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(218, 22);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // lbl_dtp
            // 
            this.lbl_dtp.AutoSize = true;
            this.lbl_dtp.Location = new System.Drawing.Point(78, 398);
            this.lbl_dtp.Name = "lbl_dtp";
            this.lbl_dtp.Size = new System.Drawing.Size(0, 16);
            this.lbl_dtp.TabIndex = 15;
            // 
            // day_time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 444);
            this.Controls.Add(this.lbl_dtp);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txt_mounth);
            this.Controls.Add(this.txt_year);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_days_of_mounth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_leap_year);
            this.Controls.Add(this.lbl_time);
            this.Controls.Add(this.lbl_day);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_daysinmounth);
            this.Controls.Add(this.cmd_leap_year);
            this.Controls.Add(this.cmd_time);
            this.Controls.Add(this.cmd_today);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "day_time";
            this.Text = "Datum und Zeit";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_today;
        private System.Windows.Forms.Button cmd_time;
        private System.Windows.Forms.Button cmd_leap_year;
        private System.Windows.Forms.Button cmd_daysinmounth;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Label lbl_day;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_leap_year;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_days_of_mounth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_year;
        private System.Windows.Forms.TextBox txt_mounth;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lbl_dtp;
    }
}

